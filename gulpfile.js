var gulp = require('gulp');
var browser = require('browser-sync');

gulp.task('hello', function () {   // 建立一个gulp的任务 第一个参数是名字 第二个是cb
	console.log('你好')
})

gulp.task('browserSync', function(){   //新建一个任务
	browser({  //配置了 browser
		files: ['**'],    // 什么文件
		server:{
			baseDir: 'app'   // 基础路径
		},
		port:8085   //端口
	})
})